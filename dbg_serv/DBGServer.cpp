//
//  DBGServer.cpp
//  dbg_serv
//
//  Created by Michael Artuerhof on 11.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

#include "DBGServer.hpp"
#include "bridged_server.hpp"
#include <stdio.h>
#include <string.h> // memset
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <pthread.h>
#include <stdlib.h>

struct thread_data {
    int sockfd;
    void *objc;
    dbg_srv_func_t fun;
    struct sockaddr_in serv_addr;
};

DBGServer::DBGServer(void *objc, int port):
_class(objc), _port(port)
{
}

DBGServer::DBGServer(dbg_srv_func_t f, int port):
_fun(f), _port(port)
{
}

DBGServer::~DBGServer() {
}

void *server_proc(void *x) {
    char buffer[256];
    struct thread_data *d = (struct thread_data *)x;
    struct sockaddr_in cli_addr;
    socklen_t clilen = sizeof(sockaddr_in);
    ssize_t n;
    while (1) {
        int newsockfd = accept(d->sockfd, (struct sockaddr *)&cli_addr, &clilen);
        if (newsockfd < 0)
            continue;
        bzero(buffer, sizeof(buffer));
        n = read(newsockfd, buffer, sizeof(buffer));
        if (n > 0) {
            if (!strncmp(buffer, DBGSERVER_EXIT_MSG, sizeof(buffer)))
                break;
            if (d->objc)
                dbg_msg_came(d->objc, buffer);
            else if (d->fun)
                d->fun(buffer);
        }
        close(newsockfd);
    }
    close(d->sockfd);
    free(d);
    pthread_exit(NULL);
}

int DBGServer::startServer() {
    struct thread_data *d = (struct thread_data*)malloc(sizeof(*d));
    bzero(d, sizeof(*d));

    d->sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (d->sockfd < 0) {
        free(d);
        return -1;
    }

    d->serv_addr.sin_family = AF_INET;
    d->serv_addr.sin_addr.s_addr = INADDR_ANY;
    d->serv_addr.sin_port = htons(_port);

    if (bind(d->sockfd, (struct sockaddr *)&d->serv_addr, sizeof(d->serv_addr)) < 0) {
        free(d);
        return -2;
    }

    if (listen(d->sockfd, 1) == -1) {
        free(d);
        return -3;
    }

    d->objc = _class;
    d->fun = _fun;

    pthread_t thread;
    pthread_create(&thread, NULL, server_proc, d);

    return 0;
}
