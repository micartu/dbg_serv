//
//  dbg_serv.h
//  dbg_serv
//
//  Created by Michael Artuerhof on 11.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for dbg_serv.
FOUNDATION_EXPORT double dbg_servVersionNumber;

//! Project version string for dbg_serv.
FOUNDATION_EXPORT const unsigned char dbg_servVersionString[];

#import "BRMServerDbg.h"
