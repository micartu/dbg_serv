//
//  BRMServerDbg.m
//  dbg_serv
//
//  Created by Michael Artuerhof on 11.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

#import "BRMServerDbg.h"
#include "DBGServer.hpp"
#include "bridged_server.hpp"

const NSInteger kDbgSrvdefaultPort = 8778;

@implementation BRMServerDbg {
    DBGServer *_srv;
    BMRServerDbgBlock_t _block;
}

- (instancetype)init {
    return [self initDebugServerWithPort:kDbgSrvdefaultPort];
}

- (instancetype)initDebugServerWithPort:(NSInteger)port {
    if (self = [super init]) {
        _srv = new DBGServer((__bridge void*)self, (int)port);
        _srv->startServer();
    }
    return self;
}

- (void)dbgMessageCame:(NSString*)msg {
    if (_block)
        _block(msg);
}

- (void)setBlockToExecute:(BMRServerDbgBlock_t)block {
    _block = block;
}

void dbg_msg_came(void *object, const char *buffer) {
    NSString *str = [NSString stringWithUTF8String:buffer];
    dispatch_async(dispatch_get_main_queue(), ^{
        [(__bridge id)object dbgMessageCame:str];
    });
}

@end
