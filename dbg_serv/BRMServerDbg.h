//
//  BRMServerDbg.h
//  dbg_serv
//
//  Created by Michael Artuerhof on 11.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const NSInteger kDbgSrvdefaultPort;

typedef void(^BMRServerDbgBlock_t)(NSString *param);

@interface BRMServerDbg : NSObject

- (instancetype)initDebugServerWithPort:(NSInteger)port;
- (void)setBlockToExecute:(BMRServerDbgBlock_t)block;

@end
