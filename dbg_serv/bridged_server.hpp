//
//  bridged_server.hpp
//  dbg_serv
//
//  Created by Michael Artuerhof on 11.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

#ifndef bridged_server_h
#define bridged_server_h

void dbg_msg_came(void *object, const char *buffer);

#endif /* bridged_server_h */
