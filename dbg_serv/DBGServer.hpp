//
//  DBGServer.hpp
//  dbg_serv
//
//  Created by Michael Artuerhof on 11.02.18.
//  Copyright © 2018 BearMonti. All rights reserved.
//

#ifndef DBGServer_hpp
#define DBGServer_hpp

#define DBGSERVER_EXIT_MSG ";exit;"

typedef void (*dbg_srv_func_t)(const char*);

class DBGServer
{
public:
    DBGServer(void *objc, int port);
    DBGServer(dbg_srv_func_t f, int port);
    virtual ~DBGServer();

    int startServer();

private:
    void *_class;
    dbg_srv_func_t _fun;
    int _port;
};

#endif /* DBGServer_hpp */
